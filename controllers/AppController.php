<?php
/**
 * Created by PhpStorm.
 * User: Навруз
 * Date: 26.07.2018
 * Time: 13:20
 */

namespace app\controllers;

use yii\filters\AccessControl;
use yii\web\Controller;

class AppController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                ],

            ],
        ];
    }
}